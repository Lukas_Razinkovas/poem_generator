"""
Artificial Pushkin

Based on markov processes

Lukas Razinkovas
"""

from __future__ import print_function
import numpy as np
from lxml import etree
from collections import Counter
import pprint
import random
import re
pp = pprint.PrettyPrinter()


def parse(filepath="tree.xml"):
    """Reads xml file with tagged poem."""
    return etree.parse(filepath).getroot()


def generate_markov_matrix(iterator):
    """Genereates markov matrix for sequence of
    language elements inside some element.
    """
    # Get possible childs
    elements = []
    names = set()
    for i in iterator:
        elements.append(i)
        names.update({j.tag for j in i})

    # Create map
    MAP = [("START", 0)]
    for nr, name in enumerate(names, 1):
        MAP.append((name, nr))

    MAP.append(("FINISH", nr+1))

    _map = dict(MAP)
    dim = len(_map)

    transition_counts = Counter()
    for sentence in iterator:
        state = "START"
        for phrase in sentence:
            transition_counts[(_map[state], _map[phrase.tag])] += 1
            state = phrase.tag

        transition_counts[(_map[state], _map["FINISH"])] += 1
        transition_counts[(_map["FINISH"], _map["FINISH"])] += 1

    matrix = np.zeros([dim, dim])
    for fr in range(dim):
        total = 0
        for to in range(dim):
            total += transition_counts[(fr, to)]
        for to in range(dim):
            matrix[to][fr] = (transition_counts[(fr, to)]/total
                              if transition_counts[(fr, to)] else 0)

    return matrix, MAP


def analyse_text():
    """Labai paprastai dviejų gylių sistemai:
    Sakinys --> Frazė --> Kalbos dalis"""
    tree = parse()
    phrases_matrix, MAP = generate_markov_matrix(tree)

    # sequence = generate_sequence(phrases_matrix, len(MAP))
    # print([MAP[i][0] for i in sequence])

    word_structures = {}
    for name, nr in MAP[1:-1]:
        matrix, MAP2 = generate_markov_matrix(
            tree.xpath("//{}".format(name)))
        word_structures[name] = (matrix, MAP2)

    return {
        "phrases": (phrases_matrix, MAP),
        "words": word_structures,
        "tree": tree
    }


def generate_sequence(matrix, _map):
    dim = len(_map)
    state = np.array([1] + [0]*(dim-1))
    sequence = []
    while state[-1] != 1:
        state = np.dot(matrix, state)
        indx = np.random.choice(len(state), p=state)
        sequence.append(indx)
        state = np.zeros(len(state))
        state[indx] = 1

    return [_map[i][0] for i in sequence[:-1]]


rules = analyse_text()


def get_case(word, word_type, parent):
    if word_type != "N":
        return word

    """Nustato linksni pagal frazes tipa"""
    transformations = {
        "NP": {
            "ius": "iai",
            "ą": "as",
            "us": "ai",
            "ens": "uo"
        },
        "VP": {
            "ė": "es",
            "is": "į",
            "ės": "es",
            "a": "ą",
            "as": "ą",
            "iai": "ius",
            "ys": "į"
        },
    }

    for fr in transformations[parent]:
        word = re.sub(fr+"$", transformations[parent][fr], word)

    return word


def generate_sentence(rules=rules):
    phrases = generate_sequence(*rules["phrases"])
    # print(phrases)
    sentence = []
    for phrase_type in phrases:
        phrase_words = generate_sequence(*rules["words"][phrase_type])
        # print(" {}:".format(phrase_type), phrase_words)
        for i in phrase_words:
            word = random.choice(
                list(rules["tree"].xpath("//{}".format(i)))).text
            sentence.append(get_case(word, i, phrase_type))

    return " ".join(sentence)


for i in range(20):
    print(generate_sentence())
